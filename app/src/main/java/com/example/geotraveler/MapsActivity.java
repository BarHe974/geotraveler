package com.example.geotraveler;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapLongClickListener, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    private SensorManager mSensorManager;
    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;
    LocationManager locationManager;
    String locationContext;
    double latitude;
    double longitude;
    Button btnEdit, btnDelete;
    Marker selectedMarker;
    public static int nbMarker;
    public static SharedPreferences mSettings;
    public static SharedPreferences.Editor editor;

    /**
     * Cette méthode est appelée quand l'activité est créée.
     *
     * @param savedInstanceState the state of the saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        /** Vérifie si la localisation de l'appareil est activée et propose une option à l'utilisateur */
        statusCheck();

        mSettings = getSharedPreferences("markers", 0);
        editor = mSettings.edit();

        /** Cette ligne permet de reset les shared preferences en cas de bug */
        //editor.clear().apply();

        nbMarker = mSettings.getInt("nbMarker", -1);
        Log.d("nbMarker create", String.valueOf(nbMarker));

        /** Action du bouton Edition */
        btnEdit = findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(v -> {
            startActivity(new Intent(this, AddMarkerActivity.class));
        });

        /** Action du bouton Supprimer */
        btnDelete = findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(v -> {
            if(selectedMarker != null){
                editor.remove("long" + selectedMarker.getId());
                editor.remove("lat" + selectedMarker.getId());
                editor.remove("title" + selectedMarker.getId());
                selectedMarker.remove();
                nbMarker -=1;
                editor.putInt("nbMarker", nbMarker);
                editor.apply();

                for(int j = 0; j<nbMarker; j++){
                    if(!mSettings.contains("longm"+j) || !mSettings.contains("latm"+j) || !mSettings.contains("titlem"+j)){
                        for(int y = 0; y<=nbMarker; y++){
                            int id = y + 1;
                            String lon = mSettings.getString("longm"+id, "");
                            String lat = mSettings.getString("latm"+id, "");
                            String title = mSettings.getString("titlem"+id, "");
                            editor.putString("longm"+j,lon);
                            editor.putString("latm"+j,lat);
                            editor.putString("titlem"+j,title);
                            editor.apply();
                            Log.d("nbMarkerSave", "id :  " + j + " " + lon +" "+ lat + " " +title);
                        }
                    }
                }
            }
        });

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Objects.requireNonNull(mSensorManager).registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        mAccel = 10f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;

    }

    /**
     * Test si la localisation est activée ou non et propose à l'utilisateur de l'activer
     */
    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Voulez-vous activer la localisation de votre appareil pour une meilleure expérience de jeux?")
                .setCancelable(false)
                .setPositiveButton("Oui", (dialog, id) -> startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNegativeButton("Non", (dialog, id) -> dialog.cancel());
        final AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Fonction permettant de gérer l'accéléromètre
     */
    private final SensorEventListener mSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            mAccelLast = mAccelCurrent;
            mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
            float delta = mAccelCurrent - mAccelLast;
            mAccel = mAccel * 0.9f + delta;
            if (mAccel > 10) {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
                Toast.makeText(getApplicationContext(), "Position reinitialisée", Toast.LENGTH_SHORT).show();
            }
        }
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    /**
     * Cette méthode est utilisé lorsque l'activité se charge, ou recharge à nouveau.
     */
    @Override
    protected void onResume() {
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        super.onResume();
    }

    /**
     * Cette méthode est appelé lorsque l'activité est sur pause.
     */
    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(mSensorListener);
        super.onPause();
    }

    /**
     * Cette méthode est appelé lorsque la Map charge
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        AddMarkerActivity.map = mMap;

        /** créer un bouton pour gérer le zoom sur la carte */
        mMap.getUiSettings().setZoomControlsEnabled(true);

        /** Vérification des permissions */
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        /** Gestion de la géolocalisation */
        mMap.setMyLocationEnabled(true);
        locationContext = Context.LOCATION_SERVICE;
        locationManager = (LocationManager) getSystemService(locationContext);
        String provider = LocationManager.GPS_PROVIDER;
        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            Log.d("localisation", latitude + " " +  longitude);
        }

        /** Chargement des points sauvegardés */
        if(nbMarker != -1){
            for(int i = 0; i<=nbMarker; i++){
                String lon = mSettings.getString("longm"+i, "");
                String lat = mSettings.getString("latm"+i, "");
                String title = mSettings.getString("titlem"+ i, "");
                LatLng m = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
                mMap.addMarker(new MarkerOptions().position(m).title(title));
                Log.d("nbMarker m"+i, lon + " " + lat + " " + title);
            }
            Log.d("nbMarker", String.valueOf(nbMarker));
        }
        mMap.setOnMapLongClickListener(this);
        mMap.setOnMarkerClickListener(this);
    }

    /**
     * Méthode permettant de gérer l'appuie long sur la map et envoie vers la page de création de points
     * @param point
     */
    public void onMapLongClick(LatLng point) {
        MarkerOptions newMarker = new MarkerOptions();
        newMarker.position(point);
        AddMarkerActivity.marker = null;
        selectedMarker = null;
        AddMarkerActivity.markerOp = newMarker;
        startActivity(new Intent(this, AddMarkerActivity.class));
    }

    /**
     * Méthode déclenché lors de la sélection d'un point
     * @param marker
     * @return
     */
    @Override
    public boolean onMarkerClick(Marker marker) {
        //Toast.makeText(this, "Current location:\n" + marker.getPosition(), Toast.LENGTH_LONG).show();
        AddMarkerActivity.marker = marker;
        selectedMarker = marker;
        return false;
    }

    /**
     * Méthode permettant l'incrémentation du nombre de points
     */
    public static void markerPlus(){
        nbMarker++;
        editor.putInt("nbMarker", nbMarker);
        editor.apply();
    }
}