package com.example.geotraveler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;


import java.text.SimpleDateFormat;
import java.util.Date;

public class AddMarkerActivity extends AppCompatActivity {
    public static MarkerOptions markerOp;
    public static Marker marker;
    public static GoogleMap map;
    public static EditText nameField;
    public static boolean edit = false;
    private transient ArrayList <Marker> list = new ArrayList<>();
    public SharedPreferences mSettings;


    /**
     * Paramètres photo
     */
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_SELECTED_IMAGE = 2;
    private ImageView imageView;
    private Bitmap imageBitmap;
    private String timeStamp;
    private String imageFileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_marker);

        ConstraintLayout constraintLayout = findViewById(R.id.map_layout);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();

        mSettings = getSharedPreferences("markers", 0);

        nameField = findViewById(R.id.MarkerNameField);
        if(marker != null){
            edit = true;
            nameField.setText(marker.getTitle());
        }

        imageView = (ImageView)this.findViewById(R.id.imageView);

        final Button btnSend = findViewById(R.id.btnSend);
        btnSend.setOnClickListener(v -> {
            editMarker();
        });

    }

    /**
     * Retour sur l'application après une prise de photo
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /**
         * Afficher la photo prise avec l'appareil photo sur l'imageview
         */

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            try {
                Bundle extras = data.getExtras();
                imageBitmap = (Bitmap) extras.get("data");
                this.imageView.setImageBitmap(imageBitmap);

                /**
                 * Enregistrement automatique de la photo sur le téléphone la photo
                 */
                timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                imageFileName = "GeoTraveler" + timeStamp;
                MediaStore.Images.Media.insertImage(getContentResolver(),imageBitmap,imageFileName,null);
                Toast.makeText(this, getString(R.string.toastPhotoEnregistre), Toast.LENGTH_LONG).show();
            }catch (Exception e){
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        /**
         * Afficher la photo sélectionnée depuis la galerie
         */
        if(requestCode == REQUEST_SELECTED_IMAGE && resultCode == RESULT_OK){
            try {
                Uri selectedImage = data.getData();
                imageView.setImageURI(selectedImage);
            }catch (Exception e){
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Méthode permettant de créer ou de modifier un point
     */
    public void editMarker(){
        if(edit){
            marker.setTitle(String.valueOf(nameField.getText()));
            map.moveCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
        }
        else{
            markerOp.title(String.valueOf(nameField.getText()));
            marker = map.addMarker(markerOp);
            map.moveCamera(CameraUpdateFactory.newLatLng(markerOp.getPosition()));
        }
        save();
        marker.showInfoWindow();


        markerOp = null;
        finish();
    }

    /**
     * Méthode permettant de sauvegarder un point lors de sa création ou lors de la modification d'un point
     */
    public void save(){
        SharedPreferences.Editor editor = mSettings.edit();

        if(edit){
            editor.putString("title" + marker.getId(), marker.getTitle());
        } else{
            editor.putString("long" + marker.getId(), String.valueOf(marker.getPosition().longitude));
            editor.putString("lat" + marker.getId(), String.valueOf(marker.getPosition().latitude));
            editor.putString("title" + marker.getId(), marker.getTitle());

            //Incrémentation du nombre de marker
            MapsActivity.markerPlus();
        }

        Log.d("nbMarker"+marker.getId(), marker.getPosition().longitude + " " + marker.getPosition().latitude + " " + marker.getTitle());
        editor.apply();
        edit = false;
    }

    /**
     *  Prendre une photo
     * @param view
     */
    public void onClickPrendrePhoto(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Ajouter une photo
     * @param view
     */
    public void onClickAjouterPhoto(View view) {
        // Accés à la gallerie du téléphone
        Intent gallerie = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(gallerie, REQUEST_SELECTED_IMAGE);
    }
}