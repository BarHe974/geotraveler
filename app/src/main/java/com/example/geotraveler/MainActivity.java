package com.example.geotraveler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final int ASK_MULTIPLE_PERMISSION_REQUEST_CODE = 100;
    public static Service service;
    private boolean isBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ConstraintLayout constraintLayout = findViewById(R.id.main_layout);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();


        /**
         * En cliquant sur le bouton Jouer l'application demande 2 persmissions, s'ils elles sont accéptées alors on ouvre une nouvelle activité
         */
        final Button btnPlay = findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener(v -> {
            askPermissions();
            if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && this.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                startActivity(new Intent(MainActivity.this, MapsActivity.class));
            }

        });

        /**
         * Dirige vers le menu option lorsque le bouton est cliqué
         */
        final Button btnOption = findViewById(R.id.btnOption);
        btnOption.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, MenuOption.class)));

        // Prépare le Service et le Homewatcher
        Intent musicItent = new Intent();
        HomeWatcher homeWatcher = new HomeWatcher(this);
        doBindService();
        musicItent.setClass(this, Service.class);
        startService(musicItent);
        // Outrepasse le bouton d’accueil sur les méthodes d’écoutes pour mettre en pause la musique lorsque l’application n’est pas active
        homeWatcher.setOnHomePressedListener(new HomeWatcher.OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                if (service != null)
                    service.pauseMusic();
            }
            @Override
            public void onHomeLongPressed() {
                if (service != null)
                    service.pauseMusic();
            }
        });
        homeWatcher.startWatch();
    }


    private ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder binder) {
            service = ((Service.ServiceBinder) binder).getService();
        }
        public void onServiceDisconnected(ComponentName name) {
            service = null;
        }
    };

    /**
     * Cette méthode est utilisé pour lier le Service
     */
    void doBindService() {
        bindService(new Intent(this, Service.class),
                serviceConnection, Context.BIND_AUTO_CREATE);
        isBound = true;
    }

    /**
     * Cette méthode est utilisé pour délier le Service
     */
    void doUnbindService() {
        if (isBound) {
            unbindService(serviceConnection);
            isBound = false;
        }
    }

    @Override
    protected void onResume(){
        super.onResume();


        if (service != null)
            service.resumeMusic();

    }

    @Override
    protected void onPause(){
        super.onPause();
        boolean isScreenOn = false;
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (pm != null)
            isScreenOn = pm.isScreenOn();
        // If the app isn't focused and the music service exists, pause it
        if (!isScreenOn && service != null)
            service.pauseMusic();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        doUnbindService();
        Intent music = new Intent();
        music.setClass(this, Service.class);
        stopService(music); // and stop it
    }

    /**
     * La fonction askPermissions demande à l'utilisateur l'accès à la géolocalisation et à l'appareil photo
     */
    private void askPermissions(){
        int cameraPermission = this.checkSelfPermission(Manifest.permission.CAMERA);
        int storagePermission1 = this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int storagePermission2 = this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
        int gpsPermission = this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        if (gpsPermission != PackageManager.PERMISSION_GRANTED
                || cameraPermission != PackageManager.PERMISSION_GRANTED
                || storagePermission1 != PackageManager.PERMISSION_GRANTED
                || storagePermission2 != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE},
                    ASK_MULTIPLE_PERMISSION_REQUEST_CODE);

        }
    }

    /**
     * Fonction déclenchée après avoir répondu aux permissions : change d'activité si les permissions sont accordées
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && this.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            startActivity(new Intent(MainActivity.this, MapsActivity.class));
        }
        else {
            Toast.makeText(getApplicationContext(), "Permissions nécessaires", Toast.LENGTH_SHORT).show();
        }
        return;
    }
}

