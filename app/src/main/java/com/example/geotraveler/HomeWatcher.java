package com.example.geotraveler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class HomeWatcher {

    private Context context;
    private IntentFilter intentFilter;
    private OnHomePressedListener listener;
    private InnerRecevier recevier;

    /**
     * Instancie un nouveau Home watcher.
     */
    public HomeWatcher(Context context) {
        this.context = context;
        intentFilter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
    }

    /**
     * Cette méthode définit le OnHomePressed listener et le récepteur pour le bouton d’accueil
     */
    public void setOnHomePressedListener(OnHomePressedListener listener) {
        this.listener = listener;
        recevier = new InnerRecevier();
    }

    public void startWatch() {
        if (recevier != null)
            context.registerReceiver(recevier, intentFilter);
    }

    /**
     * La classe représente le récepteur pour le bouton d’accueil.
     */
    class InnerRecevier extends BroadcastReceiver {
        /**
         * Cette méthode est déclenchée lorsque le bouton d'accueil est pressé et appelle l’écouteur en conséquence.
         *
         * @param context the context
         * @param intent the intent
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra("reason");
                if (reason != null && listener != null) {
                    if (reason.equals("homekey"))
                        listener.onHomePressed();
                    else if (reason.equals("recentapps"))
                        listener.onHomeLongPressed();
                }
            }
        }
    }

    /**
     * L'interface OnHomePressed listener qui sert de lien entre le home watcher et le menu principal
     */
    public interface OnHomePressedListener {
        /**
         * Cette méthode est déclenchée lorsque l'utilisateur appuie sur le bouton d'accueil
         */
        void onHomePressed();

        /**
         * Cette méthode est déclenchée lorsque l'utilisateur appuie longtemps sur le bouton d'accueil
         */
        void onHomeLongPressed();
    }
}
