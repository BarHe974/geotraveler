package com.example.geotraveler;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;


public class Service extends android.app.Service {
    private final IBinder binder = new ServiceBinder();
    MediaPlayer mediaPlayer;
    private int length = 0;


    /**
     * Instancie un nouveau Service
     */
    public Service() { }

    /**
     * Cette classe représente le classeur du service
     */
    public class ServiceBinder extends Binder {

        public Service getService() {
            return Service.this;
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    /**
     * Cette méthode est utilisée lorsque le service est créé et lance le media player.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        mediaPlayer = MediaPlayer.create(this, R.raw.genshin_impact_original_soundtrack_sun_rises_in_liyue);

    }

    /**
     * Cette méthode est utilisée au démarrage du service qui lance la musique.
     *
     * @param intent the intent
     * @param flags the flags
     * @param startId the id
     * @return the start state
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mediaPlayer != null)
            mediaPlayer.start();

        return START_NOT_STICKY;
    }


    public void pauseMusic() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            length = mediaPlayer.getCurrentPosition();
        }
    }


    public void resumeMusic() {
        if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
            mediaPlayer.seekTo(length);
            mediaPlayer.start();
        }
    }




    /**
     * Cette méthode est appelée lorsque le service est détruit
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null)
            try {
                mediaPlayer.stop();
                mediaPlayer.release();
            } finally {
                mediaPlayer = null;
            }
    }
}
