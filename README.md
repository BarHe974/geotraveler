# GeoTraveler

Voici la page git de notre projet universitaire 2020/2021 en développement mobile : GeoTraveler

## Description
GeoTraveler est une application permettant aux utilisateurs aimant voyayer et découvrir le monde de faire comme un historique de leur voyage. En outre l'application vous permettra de créer des points sur une carte en leur attribuant un titre et une photo.

## Installation
- APK : [ici](https://gitlab.com/BarHe974/geotraveler/tree/master/GeoTraveler.apk)

## Resources externes
- Pour la musique du jeu : [Youtube](https://www.youtube.com/watch?v=cA1v81xEJ90&list=PLUJ9TtKEOLByF--S-cxJEMff5JnbJQ_lU&index=5&ab_channel=Reitsuna)

## Présentation
- Diaporama en Latex (Beamer) :
    * Overleaf : https://fr.overleaf.com/read/hfjctpwnxxxg
    * Dossier Beamer : [Diapo](https://gitlab.com/BarHe974/geotraveler/tree/master/Beamer)
    * PDF : [ici](https://gitlab.com/BarHe974/geotraveler/tree/master/Beamer/Beamer_GeoTraveler.pdf)

## Crédits
Projet réalisé par Barret Hervé, Ramanitra Willy et Randrianasolo Andriniaina, étudiants en M1 Informatique à l'Université de la Réunion. (2020/2021)